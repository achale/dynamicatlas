//
// Dr Alison Hale, Lancaster University, UK is the sole author of this script footer.js which is published under the MIT License.
// See license.txt in root directory for details.
//


// *** Footer text *** //

function footer(divName){

  var n = new Date().getFullYear();

  document.getElementById(divName).innerHTML = 
      "<p style='text-align:center;color:#333;line-height:40px'>" + "Copyright&nbsp;&copy;&nbsp;2018-" + n + 
      "<style>a.foots:link{color:#333;text-decoration:underline} a.foots:visited{color:#333;text-decoration:underline} a.foots:hover{color:#b41019;text-decoration:underline}<\/style>" +
      "&nbsp;<a href='https:\/\/www.lancaster.ac.uk\/staff\/haleac\/' target='_blank' class='foots'>Dr&nbsp;Alison&nbsp;Hale<\/a>" + ",&nbsp;Lancaster&nbsp;University<\/p>";

  // *** unhide html div for page title and radio buttons ***
  document.getElementById("title").style.display = "block";
  document.getElementById("controls").style.display = "block";


}
