//
// Dr Alison Hale, Lancaster University, UK is the sole author of this script map.js which is published under the MIT License.
// See license.txt in root directory for details.
//



// "use strict";  // not all browsers support strict so they will ignore this command!


// *** get URL with layer name ***
var urlParam = function(name){
    var rx = new RegExp('[\&|\?]'+name+'=([^\&\#]+)'),
        val = window.location.search.match(rx);
    return val ? decodeURI(val[1]): undefined;
};
var layerId = urlParam('layer');
// useage https://domainname.com/index.html?layer=abc where abc is the layer group name in the drop-down list



// *** Baselayer maps ***
if ( typeof userDefinedBaseMaps==='undefined' ) { var userDefinedBaseMaps = true }
var UseDefaultBaseLayer = true,
    UseGoogleBaseLayer = true;










if ( userDefinedBaseMaps===true ) {
   var basemap1, basemap2, baseMaps; 
   basemap1 = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',{attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>', opacity:0.2});

   basemap2 = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',{attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>', opacity:0.8});

   baseMaps = {'Pale':basemap1,'Bright':basemap2};
   UseDefaultBaseLayer = false;
}
// END: edited 04/07/2019


if ( UseDefaultBaseLayer===true ){
  var basemap1, basemap2, baseMaps;
  if (typeof window.MutationObserver==='function' && UseGoogleBaseLayer===true) {
    basemap1 = L.gridLayer.googleMutant( {type: 'roadmap', opacity:1, minZoom:1, maxZoom:18,
                styles:[
                  {"featureType":"administrative","elementType":"all","stylers":[{"saturation":"-100"}]},
                  {"featureType":"administrative.province","elementType":"all","stylers":[{"visibility":"off"}]},
                  {"featureType":"landscape","elementType":"all","stylers":[{"saturation":-100},{"lightness":65},{"visibility":"on"}]},
                  {"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},
                  {"featureType":"road","elementType":"all","stylers":[{"saturation":"-100"}]},
                  {"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},
                  {"featureType":"road.arterial","elementType":"all","stylers":[{"lightness":"30"}]},
                  {"featureType":"road.local","elementType":"all","stylers":[{"lightness":"40"}]},
                  {"featureType":"transit","elementType":"all","stylers":[{"saturation":-100},{"visibility":"simplified"}]},
                  {"featureType":"water","elementType":"geometry","stylers":[{"saturation":-100},{"lightness":65},{"visibility":"on"}]},
                  {"featureType":"water","elementType":"labels","stylers":[{"visibility":"off"}]}]
              } );
    basemap2 = L.gridLayer.googleMutant( {type: 'roadmap', opacity:1, minZoom:1, maxZoom:18} );
    baseMaps = {'Greyscale':basemap1,'Colour':basemap2};                  // list of basemaps and their names (default map is first in list)
  } else {
    if ( typeof window.MutationObserver!=='function' ) {
      console.log ("Google maps cannot be used as your web browser does not support MutationObserver; by default OSM is used instead.");
    }
    basemap1 = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',{attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>', opacity:0.6});

    basemap2 = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',{attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>', opacity:1.0});
    baseMaps={'Pale':basemap1,'Bright':basemap2};
  }
}
if ( typeof InitialMapCenterLatLng==='undefined' ) {
  var InitialMapCenterLatLng = [51.4826, 0.0077];
}
if ( typeof InitialmapZoom==='undefined' ) {
  var InitialmapZoom = 8;
}
var loadMap = ( ( typeof secondMap!=='undefined' && secondMap===true ) ? basemap2 : basemap1 );
var map = new L.Map('mapid',{center: new L.LatLng(InitialMapCenterLatLng[0],InitialMapCenterLatLng[1]),zoom:InitialmapZoom, layers:loadMap, zoomControl:false, zoomSnap: 0.25});



// *** Build and associate each overlay with its respective style ***
var geodata = [];
function buildLayers() {
  function getMap(i,j,k,len) {
    var f = function(e) {
      var mychild = new L.geoJson(AA[i].toGeoJSON(), {
                                        color:              layerdata[i].mapStyles.color,         // leafletjs option
                                        fillOpacity:        layerdata[i].mapStyles.fillOpacity,   // leafletjs option
                                        geodataID:          k,
                                        geojsongeom:        layerdata[i].geojsongeom,
                                        geojsonname:        layerdata[i].geojsonname[j],
                                        i:                  i,
                                        layerName:          layerdata[i].layerName[j],
                                        onEachFeature:      onEachFeature,                        // leafletjs option
                                        opacity:            layerdata[i].mapStyles.opacity,       // leafletjs option
                                        pointToLayer:       onEachPoint(i,j,k),                   // leafletjs option
                                        radioButtonValue:   layerdata[i].radioButtonValue,
                                        smoothFactor:       layerdata[i].mapStyles.smoothFactor,  // leafletjs option
                                        style:              choropleth(i,j),                      // leafletjs option
                                        subsetOfLayers:     layerdata[i].subsetOfLayers,
                                        timeData:           layerdata[i].timeData,
                                        timeRefId:          layerdata[i].timeRefId,
                                        weight:             layerdata[i].mapStyles.weight         // leafletjs option
      });
      layerCount = layerCount + 1;
      mychild.on('data:loaded', gotLayers(k,layerCount,len,mychild));
    };
    return f;
  }
  var layerCount = 0,
      AA = [],
      k = 0,
      len = 0;
  for (var x=0; x<layerdata.length; x++) {
    layerdata[x].timeRefId = len;
    if (typeof layerdata[x].colgrades.Num!=='undefined') {
      layerdata[x].colgrades.Vals = makeHEXcolors( layerdata[x].colgrades.Vals,layerdata[x].colgrades.Num );
    }
    len = len + layerdata[x].layerName.length; // compute total number of layers
  }
  for (var i=0; i<layerdata.length; i++) {
    layerdata[i].i = i;
    AA[i] = new L.GeoJSON.AJAX( layerdata[i].filename );
    for (var j=0; j<layerdata[i].layerName.length; j++) {
      AA[i].on('data:loaded', getMap(i,j,k,len));
      k = k+1;
    }
  }
}
function gotLayers(k,count,len,layer){  // run functions that  depends on geodata[count] being loaded
  geodata[k] = layer;
  if (count===len) {
    var overlayMaps = overlayCall();                                                          // associate layers with their names for L.control
    L.control.layers(baseMaps, overlayMaps, {collapsed:true, position:'topleft'}).addTo(map); // add all layers to leaflet dropdown menu control
    createSlider();
    createRadioButtons('control1radioButtons', radioOverLayerNames,   'layer',     'radioButton'    ,true , false);
    createRadioButtons('control2radioButtons', Object.keys(baseMaps), 'baselayer', 'radioButtonMap' ,false, true );
    createRadioButtons('controlOptionalDropDown', ['placeholder'], 'layertype', 'radioButton', true, false);
    document.getElementById('controlOptionalDropDown').style.display = 'none'; // hidden place holder for when a layer wants to use a subset dropdown menu
  }
}
function shinyLayers(){
  var i=0, k=0, x=0, len=0;
  layerdata[x].timeRefId = len;
  if (typeof layerdata[x].colgrades.Num!=='undefined') {
    layerdata[x].colgrades.Vals = makeHEXcolors( layerdata[x].colgrades.Vals,layerdata[x].colgrades.Num );
  }
  setTimeout(function(){ 
    for (var j=0; j<layerdata[i].layerName.length; j++) {
      geodata[j] = new L.geoJson( shinyGeoData, {
                                        color:              layerdata[i].mapStyles.color,         // leafletjs option
                                        fillOpacity:        layerdata[i].mapStyles.fillOpacity,   // leafletjs option
                                        geodataID:          k,
                                        geojsongeom:        layerdata[i].geojsongeom,
                                        geojsonname:        layerdata[i].geojsonname[j],
                                        i:                  i,
                                        layerName:          layerdata[i].layerName[j],
                                        onEachFeature:      onEachFeature,                        // leafletjs option
                                        opacity:            layerdata[i].mapStyles.opacity,       // leafletjs option
                                        pointToLayer:       onEachPoint(i,j,k),                   // leafletjs option
                                        radioButtonValue:   layerdata[i].radioButtonValue,
                                        smoothFactor:       layerdata[i].mapStyles.smoothFactor,  // leafletjs option
                                        style:              choropleth(i,j),                      // leafletjs option
                                        subsetOfLayers:     layerdata[i].subsetOfLayers,
                                        timeData:           layerdata[i].timeData,
                                        timeRefId:          layerdata[i].timeRefId,
                                        weight:             layerdata[i].mapStyles.weight         // leafletjs option
      });
      k = k+1;
    }
  }, 100);
  setTimeout(function(){ 
    var overlayMaps = overlayCall();                                                          // associate layers with their names for L.control
    L.control.layers(baseMaps, overlayMaps, {collapsed:true, position:'topleft'}).addTo(map); // add all layers to leaflet dropdown menu control
    createSlider();
    createRadioButtons('control1radioButtons', radioOverLayerNames,   'layer',     'radioButton'    ,true , false);
    createRadioButtons('control2radioButtons', Object.keys(baseMaps), 'baselayer', 'radioButtonMap' ,false, true );
    createRadioButtons('controlOptionalDropDown', ['placeholder'], 'layertype', 'radioButton', true, false);
    document.getElementById('controlOptionalDropDown').style.display = 'none'; // hidden place holder for when a layer wants to use a subset dropdown menu
    setTimeout(function(){ chooseLayer(layerdata[0].radioButtonValue); }, 300);
  }, 600);
  
}
function buildWhat(){
  ( typeof addShinyLayers!=='undefined') ? shinyLayers() : buildLayers();
}
buildWhat();


// *** callback ordering idea! ***
//function Af(newLayer, callback ){
//    var oldLayer=newLayer; setTimeout(function(){ console.log("First"); callback(oldLayer); }, 4000);
//}
//function Bf(oldLayer){ 
//  var index=oldLayer;console.log("Second");
//}
//Af("Hi", function(n){Bf(n)} );  
//
// *** refilter (from L.GeoJSON.AJAX) gets features from object AA ***
//var AA = new L.GeoJSON.AJAX(layerdata[0].filename); AA.options.mykey = "AA"; console.log(AA)
//var CC = AA.once('data:loaded', getMap);
//function getMap(e) {
//  var BB = AA.refilter(function(e){ console.log(e); return AA; });
//  return BB;
//}


// *** construct array of map border/boundary layers ***
var borders = [];     // array of borders
var ShinyBorder = (typeof shinyBoundry!=='undefined') ? true : false;
function buildBoundaries() {
  var data = [],
      style = {color:'#555', weight:2}, // default border style
      past = 0,
      k = 0;
  for ( var i=0; i<layerdata.length; i++ ) {
    if ( typeof layerdata[i].mapBoundary!=='undefined' ) {
      if ( i>0 ) { past = i-1 }
      if ( i>0 &&
           typeof layerdata[past].mapBoundary             !=='undefined' &&
           typeof layerdata[past].mapBoundary.filename    !=='undefined' &&
           typeof layerdata[past].mapBoundary.borderStyle !=='undefined' &&
           typeof layerdata[i].mapBoundary.borderStyle    !=='undefined' && 
           layerdata[i].mapBoundary.filename           === layerdata[past].mapBoundary.filename &&
           layerdata[i].mapBoundary.borderStyle.color  === layerdata[past].mapBoundary.borderStyle.color &&
           layerdata[i].mapBoundary.borderStyle.weight === layerdata[past].mapBoundary.borderStyle.weight
         )
      {
        data[i] = data[past];  // borders[i] points to borders[i-1] i.e. borders[i] is not a clone of borders[i-1] (n.b. past=i-1)
      } else {
        if ( typeof layerdata[i].mapBoundary.borderStyle!=='undefined' ) { style = layerdata[i].mapBoundary.borderStyle } // need 'if' statement when i=0
        data[i]=( ShinyBorder===true ) ? new L.geoJson(shinyBoundry,{style:style}) : new L.GeoJSON.AJAX(layerdata[i].mapBoundary.filename,{style:style});
      }
    } else {
      data[i] = undefined;
    }
    for ( var j=0; j<layerdata[i].layerName.length; j++ ) { 
      borders[k] = data[i];
      k = k+1;
    }
  }
}
buildBoundaries();


// *** get layer names for radio/dropdown ***
var radioOverLayerNames = [layerdata[0].radioButtonValue];
function getRadioOverLayerNames() {
  for (var i=1; i<layerdata.length; i++) {
    radioOverLayerNames.push(layerdata[i].radioButtonValue);
  }
}
getRadioOverLayerNames();


// *** when needed (ie. if colgrades.Num exists) put extra colours in colour lists (ie. colgrades.Vals) ***
function makeHEXcolors(rng,len) {
  var x = chroma.scale(rng),
      a = [];
    for (var i=0; i<len; i++) {
      a[i] = x(i/(len-1)).hex();
    }
  return a;
}


// *** feature and legend colours ***
var legend = L.control({position: 'bottomright'}),
    colInt = 0,
    colIni = 0,
    colVal = [],
    colUserDef,
    dPlace = 1e12;                            // used to remove floating point errors (e.g. on map legend)
function getColor(d, int0, int, val, noDataCol) {
  if ( typeof d==='undefined' ) { return noDataCol } // when region has no value its colour is defined as noDataColour
  for (var i=val.length-1; i>=0; i--) {
    if ( Math.round(d*dPlace)/dPlace >= Math.round((int0 + int*i)*dPlace)/dPlace ) { return val[i] }
  }
}
function makelegend() {
    var div = L.DomUtil.create('div', 'legend'),
        col,
        x,
        len = colVal.length - 1;
    if (typeof colTit!=='undefined'){ div.innerHTML += colTit + '<br>' }
    for (var i=0; i<(len+1); i++) {           // generate label with colored square for each interval
        x = ((i!==0) ? (colIni + colInt*i) : colIni);
        col = getColor(x, colIni, colInt, colVal, null);
        div.innerHTML += '<span style="background:' + col + '; color:' + col + '">__</span> ' +
                          Math.round(x*dPlace)/dPlace + ((i!==len) ? '&ndash;' + Math.round(colInt*(i+1)*dPlace)/dPlace + '<br>' : '+');
    }
    return div;
}
function getColorUserDef(d, colBounds, val, noDataCol) {
  if ( typeof d==='undefined' ) { return noDataCol } // when region has no value its colour is defined as noDataColour
  for (var i=val.length-1; i>=0; i--) {
    if ( Math.round(d*dPlace)/dPlace >=  colBounds[i]) { return val[i] }
  }
}
function makelegendUserDef() {
    var div = L.DomUtil.create('div', 'legend'),
        len = colVal.length - 1,
        col;
    if (typeof colTit!=='undefined'){ div.innerHTML += colTit + '<br>' }
    for (var i=0; i<(len+1); i++) {           // generate label with colored square for each interval
        col = colVal[i];
        div.innerHTML += '<span style="background:' + col + '; color:' + col + '">__</span> ' +
                          Math.round(colUserDef[i]*dPlace)/dPlace;
        div.innerHTML += ((i!==len) ? '&ndash;' + Math.round(colUserDef[i+1]*dPlace)/dPlace + '<br>' : '+');
    }
    return div;
}
function choropleth(i,j) {
  var f = function(feature) {
    if (typeof layerdata[i].colgrades.userDefined === 'undefined'){
      return { fillColor: getColor(feature.properties[ layerdata[i].geojsonname[j] ], layerdata[i].colgrades.Inis, layerdata[i].colgrades.Ints, layerdata[i].colgrades.Vals, layerdata[i].noDataColour) };
    } else {
      return { fillColor: getColorUserDef(feature.properties[ layerdata[i].geojsonname[j] ], layerdata[i].colgrades.userDefined, layerdata[i].colgrades.Vals, layerdata[i].noDataColour) }; 
    }
  };
  return f;
}


// *** point to layer - style circle markers ***
var currentHighlight,     // stores current layer features and options
    layerFeatureID;       // stores current object id
customCircleMarker = L.CircleMarker.extend({ options:{shiftCoords:[0,0]} });
function hltPointAddPlot(feature,ops,k) {
  if (ops.timeData.timeseries===true) {
    var vs = getKeyValues(feature.properties),
        highlightMark = (typeof ops.timeData.highlight!=='undefined' && typeof ops.timeData.highlight.field!=='undefined') ? feature.properties[ops.timeData.highlight.field] : undefined;
    PlotTimeSeries(true, highlightMark, feature.properties[ops.geojsongeom], Object.keys(feature.properties), vs, k, ops);
  }
  currentHighlight = { feature:feature, options:ops }; 
  layerFeatureID = feature.properties[ops.geojsongeom]; // 18-05-2021 layerFeatureID = feature.id;
}
function onEachPoint(i,j,k) {
  var highlightPoint = function(feature, latlng) {
    if (typeof feature.options!=='undefined') { return }  // function not used for choropleth layers instead highlightFeature() makes highlights & popups
    var shift = (typeof feature.properties.coordShift!=='undefined' && feature.properties.coordShift!=="") ? feature.properties.coordShift : [0,0],
        ops = layerdata[i],
        style = {color:ops.mapStyles.color, weight:ops.mapStyles.weight, fillOpacity:ops.mapStyles.fillOpacity, radius:ops.mapStyles.radius, shiftCoords:shift},
        val = feature.properties[ops.geojsonname[j]],
        popval = feature.properties[ops.geojsonExtraInfo],
        txt = (typeof ops.layerName[j]!=='undefined' ? '<div id=\"infoDivHeader\">'+ops.layerName[j]+'</div>' : '') +
              '<div id=\"infoDivTxt\">' +
                (typeof ops.geojsongeom!=='undefined' ?  ops.regionNames.area + ': ' + feature.properties[ops.geojsongeom] + '<br>' : '' ) +
                ((typeof ops.geojsonExtraInfo!=='undefined' && typeof popval!=='undefined') ? ops.geojsonExtraInfo+': '+popval.replace(/_/g,':')+'<br>' : '') +
                (typeof val==='undefined' ? '<br>No data reported' : '<span id=\"infoDivbold\">' + val + '</span> ' + ops.units.html) +
              '</div>',
        circle;
    if (ops.popupBox===true){
      circle = new customCircleMarker(latlng, style).bindPopup(txt);
      circle.on('click', function(){ resetHighlight(k); circle.setStyle(ops.featurehltStyle); hltPointAddPlot(feature,ops,k) });//Highlight marker, add plot
    } else {
      circle = new customCircleMarker( latlng, style );
      circle.on('click', function(){ resetHighlight(k); circle.setStyle(ops.featurehltStyle); hltPointAddPlot(feature,ops,k); infoDiv.update( txt ) });
    }
    return circle;
  };
  return highlightPoint;
}
function adjustPosPnts(e) { // move position of points relative to zoom so they don't overlap - uses shiftCoords column from csv/geojson to adjust coords
  function moveCoords(layer) { 
    if ( typeof layer._latlng==='undefined' || typeof layer.options.shiftCoords==='undefined') { return }
    var pix = map.latLngToContainerPoint( map.getCenter() ), // convert to pixels
        latLngP = map.containerPointToLatLng( pix ),         // convert to latlngs
        latLngX = map.containerPointToLatLng( [ pix.x+layer.options.radius, pix.y ] ),
        latLngY = map.containerPointToLatLng( [ pix.x, pix.y+layer.options.radius ] ),
        newPos = new L.LatLng( layer.feature.geometry.coordinates[1] + layer.options.shiftCoords[1]*(latLngP.lat-latLngY.lat),
                               layer.feature.geometry.coordinates[0] + layer.options.shiftCoords[0]*(latLngP.lng-latLngX.lng) );
    layer.setLatLng( newPos );
  }
  var grp;
  map.eachLayer(function(layer){  if(typeof layer.options.i!=='undefined'){grp=layer.options.i}  }); // find layer group (with same time plot)
  for (var i=0; i<geodata.length; i++) {
    if (geodata[i].options.i===grp) { geodata[i].eachLayer( moveCoords ) }
  }
}
map.on('zoomend', adjustPosPnts);



// *** on each feature - style raster/choropleth layers ***




var infoDiv = L.control();
infoDiv.update = function (txt) {                           // update infoDiv which displays map values (alternative to Popup box)
  this._container.innerHTML = (typeof txt!=='undefined' ? txt : 
                                      '<div id=\"infoDivTxt\" style=\"font-weight:bold\">Click map for<br>outcome values</div>');
};
function highlightFeature(e) {
  if (typeof this.options.geodataID==='undefined') {return}  // function not used for circleMarker layers instead onEachPoint() makes highlights & popups
  var ops = layerdata[this.options.i],
      val = this.feature.properties[this.options.geojsonname],
      popval = this.feature.properties[ops.geojsonExtraInfo],
      vs, highlightMark,
      txt='<div id=\"infoDivHeader\">'+this.options.layerName+'</div>'+ '<div id=\"infoDivTxt\">'+
            (typeof this.options.geojsongeom!=='undefined' ? ops.regionNames.area+': '+ this.feature.properties[this.options.geojsongeom]+'<br>' : '' ) +
            ((typeof ops.geojsonExtraInfo!=='undefined' && typeof popval!=='undefined') ? ops.geojsonExtraInfo+': '+ popval +'<br>' : '') +
            (typeof val==='undefined' ? '<br>No data reported' : '<span id=\"infoDivbold\">' + val + '</span> ' + ops.units.html) +
          '</div>';
  if (ops.popupBox===true){
    this.bindPopup( txt );
    this.openPopup();
  } else {
    infoDiv.update( txt );
  }






















  if (typeof currentLayer!=='undefined') { resetHighlight(currentLayer.layer.options.geodataID) }
  this.setStyle(ops.featurehltStyle);
  this.bringToFront();
  //if (L.Browser.ie===false && L.Browser.opera12===false && L.Browser.edge===false) { this.bringToFront() }
  if (this.options.timeData.timeseries===true) {
    vs = getKeyValues(this.feature.properties);
    highlightMark = (typeof this.options.timeData.highlight!=='undefined' && typeof this.options.timeData.highlight.field!=='undefined') ? this.feature.properties[this.options.timeData.highlight.field] : undefined;
    PlotTimeSeries(true, highlightMark, this.feature.properties[this.options.geojsongeom], Object.keys(this.feature.properties), vs, this.options.geodataID, this.options);
  }
  currentHighlight = e.target;
  layerFeatureID = e.target.feature.properties[this.options.geojsongeom]; // 18-05-2021 layerFeatureID = e.target.feature.id;  //layerFeatureID = this._leaflet_id;
}
function getKeyValues (obj) {               // need this function as IE doesn't support Object.values() it only supports Object.keys()
  var vals = [];
  for (var i=0; i<Object.keys(obj).length; i++) {
    vals.push(obj[Object.keys(obj)[i]]);
  }
  return vals;
}
function resetHighlight(index) {
      geodata[index].eachLayer( function(l){ geodata[index].resetStyle(l) } );
}
function onEachFeature(feature,layer) {
  layer.on({ click: highlightFeature });
}


// *** link overlay layers to thier names ***
function overlayCall() {
  var overlays = {};
  for (var i=0; i<geodata.length; i++) {
    overlays[[geodata[i].options.layerName]] = geodata[i];
  }
  return overlays;
}


// *** add/remove legend, graph and feature styling when layers are changed  ***
var currentLayer,
    currentInfoDiv,
    currentLegend;
    
function addNewLayer_A(newLayer, callback ){  // when adding a layer update global variable 'currentLayer' before removing previous layer
    var oldLayer=currentLayer;
    currentLayer=newLayer;                    // store current layer 'e'
    callback(oldLayer);
}
function addNewLayer_B(oldLayer){             // after updating global variable 'currentLayer' it is then safe to remove previous layer
  var index;
  if (typeof oldLayer!=='undefined') {
    index=oldLayer.layer.options.geodataID;
    resetHighlight(index);
    if (index!==currentLayer.layer.options.geodataID) { map.removeLayer(geodata[index]) }
  }
}
var markerLayer; // holder for marker layer for each overlay
function addMarker(i){ // for each layer add new marker if defined 
  var latlng,
      m = layerdata[i].layerMarker,
      txt = "";
  if ( typeof m==='undefined' || typeof m.latlng==='undefined' ) { 
    if ( map.hasLayer(markerLayer) ) { map.removeLayer(markerLayer) } 
    return;
  } else {
    latlng = m.latlng;
  }
  if ( typeof m.popuptxt!=='undefined' ) { txt=m.popuptxt }
  if ( !map.hasLayer(markerLayer) ) { 
    markerLayer = L.marker( latlng ).bindPopup( txt );
    map.addLayer(markerLayer);
  } else {
    map.removeLayer(markerLayer);
    markerLayer = L.marker( latlng ).bindPopup( txt );
    map.addLayer(markerLayer);
  }
}
var borderLayer; // current boundary layer
function addExternalBoundary(i){
  if ( typeof borders[i]==='undefined') {   // no border data for current map layer
    if ( typeof borderLayer!=='undefined' && map.hasLayer(borderLayer) ) { map.removeLayer(borderLayer) }
    return;
  }
  if ( !map.hasLayer(borderLayer) ) {
    borderLayer = borders[i];
    map.addLayer(borderLayer);
    borderLayer.bringToBack();  // borderLayer.bringToFront();
  } else {
    map.removeLayer(borderLayer);
    borderLayer = borders[i];
    map.addLayer(borderLayer);
    borderLayer.bringToBack(); // borderLayer.bringToFront();
  }
}
function addNewLayer(e) {
  var c = currentHighlight,
      lyr, kys, vs, txt, val, popval, highlightMark,
      ops = layerdata[e.layer.options.i],
      index = 0;
  lyr = e.layer.getLayers();
  kys = Object.keys(e.layer._layers);
  if ( typeof currentLayer==='undefined' || cleared === true) {   // set map position and zoom when first layer is loaded or when the radioButtonValue changes
    //map.setView( new L.LatLng(ops.mapPosition.centerLatLng[0], ops.mapPosition.centerLatLng[1]), ops.mapPosition.zoom );
    cleared = false;
    map.closePopup();
//  } else if ( currentLayer.layer.options.radioButtonValue!==e.layer.options.radioButtonValue ) {
  } else if ( currentLayer.layer.options.i!==e.layer.options.i ) {
    //map.setView( new L.LatLng(ops.mapPosition.centerLatLng[0], ops.mapPosition.centerLatLng[1]), ops.mapPosition.zoom );
    currentHighlight = undefined;
    map.closePopup();
//  } else if ( currentLayer.layer.options.radioButtonValue===e.layer.options.radioButtonValue && 
  } else if ( currentLayer.layer.options.i===e.layer.options.i && 
              typeof c!=='undefined' && typeof c.feature.properties[e.layer.options.geojsongeom]!=='undefined') {
    val = c.feature.properties[e.layer.options.geojsonname];
    popval = c.feature.properties[ops.geojsonExtraInfo];
    txt = 
        '<div id=\"infoDivHeader\">'+e.layer.options.layerName+'</div>' + 
        '<div id=\"infoDivTxt\">'+
          (typeof e.layer.options.geojsongeom!=='undefined' ? ops.regionNames.area+': '+c.feature.properties[e.layer.options.geojsongeom]+'<br>' : '' ) +
          ((typeof ops.geojsonExtraInfo!=='undefined' && typeof popval!=='undefined') ? ops.geojsonExtraInfo+': '+popval.replace(/_/g,':')+'<br>' : '') +
          (typeof val==='undefined' ? '<br>No data reported' : '<span id=\"infoDivbold\">' + val + '</span> ' + ops.units.html) +
        '</div>';
  }
  if ( typeof currentLayer==='undefined' || currentLayer.layer.options.i!==e.layer.options.i ) { setTimeout( function(){ adjustPosPnts() }, 200) }
  if (timeSliderListener===false) {  // remove previous layer when triggered by leafletjs menu (tick box)
    if (typeof currentLayer!=='undefined') {
      index=currentLayer.layer.options.geodataID;
      setTimeout(function(){ resetHighlight(index) }, 20 );
      if (index!==e.layer.options.geodataID) { setTimeout(function(){map.removeLayer(geodata[index])}, 40 )  }
    }
    currentLayer = e;                                       // store current layer 'e'
  }
  if (timeSliderListener===true) {  // remove previous layer when triggered by html timeslider
    addNewLayer_A(e, function(n){   // use callback so variable 'currentLayer' is updated before removal of old layer fires removeOldLayer()
      addNewLayer_B(n);
    });  
    timeSliderListener = false;
  }
  if ( typeof c!=='undefined' && c.options.timeRefId!==e.layer.options.timeRefId ) { layerFeatureID=undefined }
  if (typeof layerFeatureID!=='undefined' && typeof c!=='undefined' && c.options.timeData.timeseries===true && e.layer.options.timeData.timeseries===true) {
    for (var i=0; i<lyr.length; i++) {                  // find correct feature to highlight on new layer
      if (lyr[i].feature.properties[e.layer.options.geojsongeom]===layerFeatureID){ //18-05-2021 lyr[i].feature.id===layerFeatureID)
        e.layer._layers[ kys[i] ].setStyle(ops.featurehltStyle);
        e.layer._layers[ kys[i] ].bringToFront();
        break;
      }
    }
    vs = getKeyValues(c.feature.properties);
    highlightMark = (typeof c.options.timeData.highlight!=='undefined' && typeof c.options.timeData.highlight.field!=='undefined') ? c.feature.properties[c.options.timeData.highlight.field] : undefined;
    PlotTimeSeries(true, highlightMark, c.feature.properties[c.options.geojsongeom], Object.keys(c.feature.properties), vs, e.layer.options.geodataID, e.layer.options);
  }
  if (typeof layerFeatureID==='undefined' && typeof e.layer.options.timeData!=='undefined' && e.layer.options.timeData.timeseries===true) {
    PlotTimeSeries(false, null, null, null, null, e.layer.options.geodataID, e.layer.options);  // only plot mean data
  }  
  if (ops.legend===true) {                // add legend
    colInt = ops.colgrades.Ints;
    colIni = ops.colgrades.Inis;
    colVal = ops.colgrades.Vals;
    colTit = ops.colgrades.legtitle;
    colUserDef = ops.colgrades.userDefined;
    if (typeof colUserDef==='undefined') {
      legend.onAdd = makelegend;
    } else {
      legend.onAdd = makelegendUserDef;
    }
    currentLegend = legend;
    legend.addTo(map);
  }
  if (ops.popupBox===false) {            // add infoDiv to display map values (instead of putting vals in a Popup box)
    infoDiv.onAdd = function(){ return L.DomUtil.create('div', 'infoDiv') }; // create div, class=infoDiv,
    currentInfoDiv = infoDiv;
    infoDiv.addTo(map);
    ( typeof txt!=='undefined' ) ? infoDiv.update(txt) : infoDiv.update();
  }
  addMarker(e.layer.options.i);
  addExternalBoundary(e.layer.options.geodataID);     // add external boundary
  
  
  
  
  
  if ( radioNotDropdown===true)  { document.getElementById(e.layer.options.radioButtonValue).checked = true }
  if ( radioNotDropdown===false) { document.getElementById("control1dropdown").value = e.layer.options.radioButtonValue }
}
function removeOldLayer(e) {
  var ops = layerdata[currentLayer.layer.options.i];
  if (typeof currentLegend!=='undefined'){
    if (ops.legend===false || currentLayer.name===e.name) {                                // remove legend
      map.removeControl(currentLegend);
    }
  }
  if (typeof currentInfoDiv!=='undefined'){
    if (ops.popupBox===true || currentLayer.name===e.name) {                                // remove infoDiv
      map.removeControl(currentInfoDiv);
      cleared=true;
    }
  }






  if (currentLayer.layer.options.timeData.timeseries===false || currentLayer.name===e.name) {                     // remove timeseries and slilder
    layerFeatureID = undefined;
    ClearTimeSeriesPlot(); 
    displaySlider(false,null,null,null);
  }
  // remove all radio checks, reset dropdown menu, remove marker and border
  if (currentLayer.name===e.name){
    if (radioNotDropdown===true)  { document.getElementById(e.layer.options.radioButtonValue).checked=false }
    if (radioNotDropdown===false) { document.getElementById("control1dropdown").options[0].selected = 'selected' }
    if (typeof borderLayer!=='undefined') { map.removeLayer(borderLayer) }
    if (typeof markerLayer!=='undefined') { map.removeLayer(markerLayer) }
  }
}
function switchBaseLayer(e) {                       // assumes each radio button in html has one of the keys listed in the global varible baseMaps
  for (var i=0; i<Object.keys(baseMaps).length; i++) {
    if (e.name===Object.keys(baseMaps)[i]) {
      document.getElementById(Object.keys(baseMaps)[i]).checked = true;
    }
  }
}

map.on('overlayadd', addNewLayer);
map.on('overlayremove', removeOldLayer);
map.on('baselayerchange', switchBaseLayer);


// *** plot time series ***
var timeseriesChart;
/*
var ChartjsPluginVerticalLine = {
  drawVerticalLine: function (c, id) {
    var xpos = c.getDatasetMeta(0).data[id.t]._model.x;  // (0) means use 1st dataset and id.t is the time at which the vertical line is plotted
    c.chart.ctx.beginPath();
    c.chart.ctx.strokeStyle = id.c;                      // id.c equals timePlot.HighlightColour
    c.chart.ctx.moveTo(xpos, c.scales['y-axis-0'].top);
    c.chart.ctx.lineTo(xpos, c.scales['y-axis-0'].bottom);
    c.chart.ctx.stroke();
  },
  afterDatasetsDraw: function (chart, a) {
    if (typeof chart.config.lineAtIndex!=='undefined') { this.drawVerticalLine(chart, chart.config.lineAtIndex) }
  }
};
*/

function checkForCanvas() {
  var test = document.createElement('canvas');                //try to create canvas element
  var check = !!(test.getContext && test.getContext('2d'));   // true if <canvas> supported (nb if .getContext==undefined => !undefined=true => !true=false)
  if ( check===true) {
    var c = test.getContext('2d');
    check = (typeof c.fillText==='function') ? true : false;
  }
  return check;
}
var canvasExists = checkForCanvas();

function checkForInputRange() {
  var t = document.createElement('input'),
      check;
  t.type = "range"; 
  check = (t.type==="range") ? true : false;  // check if object has type range
  return check;                               // true if browser supports <input type=range> element
}
var rangeExists = checkForInputRange();
//if ( canvasExists===true ) { Chart.plugins.register(ChartjsPluginVerticalLine) } // only needed when it is possible to plot timeseries on <canvas>
//var origLineDraw = Chart.controllers.line.prototype.draw;       // original draw function for line chart applied after drawing highlight rectangle behind chart
Chart.defaults.customLineChart = Chart.defaults.line;           // need to reset to defaults otherwise Shiny has recursive error when re-doing from form
/*
var customLineChart = Chart.controllers.line.extend({           // extend line chart to override native line-draw function
  draw : function(ease) {
    Chart.controllers.line.prototype.draw.call(this, ease);
    var c = this.chart,
        hltXrange = c.config.data.hltXrange;
    if ( typeof hltXrange.highlight!=='undefined' ) {
      var xaxis = c.scales['x-axis-0'],
          yaxis = c.scales['y-axis-0'];
      c.chart.ctx.save();
      c.chart.ctx.fillStyle = hltXrange.highlight.colorFill;
      xR = (typeof hltXrange.highlight.xRight!=='undefined') ? xaxis.getPixelForValue(hltXrange.highlight.padX) : xaxis.right;
      xL = (typeof hltXrange.highlight.xLeft !=='undefined') ? xaxis.getPixelForValue(hltXrange.highlight.padX) : xaxis.left;
      c.chart.ctx.fillRect(xL, yaxis.top, xR-xL, yaxis.bottom-yaxis.top); // hightlight region
      c.chart.ctx.restore();
    }
    origLineDraw.apply(this, arguments);   // apply original draw function to line chart
  }
});
Chart.controllers.derivedLine = customLineChart;
*/

function PlotTimeSeries(twolines, hltMark, label4subregion, labels, subregionVals, geodataID, ops) {
  if ( canvasExists===false ) { return }                                  // only plot if canvas exists
  var layerOps = layerdata[ops.i],
      myhltXrange = ops.timeData,
      timeRefIndex = ops.timeRefId,
      plot = layerOps.timePlot,
      meanVals = layerOps.meandata,nullMean,
      dataMean, dataY, dataCIL, dataCIU, data = [],
      bgdc2=[], bdrc1=[], bdrc2=[], bdrc3=[], pr=[],
      xlabs=[], xticks=[], subregionValsY=[], subregionCIL=[], subregionCIU=[],
      ymax = 0,
      num = 0,
      ctx = document.getElementById('timeseriesid').getContext('2d'),     // this assumes canvas id=timeseriesid is defined in html
      title = (typeof ops.timeData.highlight!=='undefined') ? { display:true, text:ops.timeData.highlight.title,  padding:0, fontStyle:'normal'} : undefined,
      displayXlabel = (typeof layerOps.units.xlab!=='undefined') ? true : false,
      j = 0;
  if (typeof meanVals==='undefined') { meanVals = [] }
  for (var n=ops.timeData.timeseriesMin; n<=ops.timeData.timeseriesMax; n=n+ops.timeData.timeseriesStep) { xlabs.push(n) }
  if ( twolines===true ) {
    for (var n=0; n<labels.length; n++) {                     // match y-values with x-axis labels
      num = labels[n].split(".");                            
      if( !isNaN(parseFloat(num[0])) && isFinite(num[0]) ) {  // valid x-axis labels always start with a number
        for (var m=0; m<xlabs.length; m++) {
          if      (labels[n]===xlabs[m].toString()) { subregionValsY.push(subregionVals[n]); break }
          else if (num[0]===xlabs[m].toString() && num[1]==="L"){ subregionCIL.push(subregionVals[n]); break }
          else if (num[0]===xlabs[m].toString() && num[1]==="U"){ subregionCIU.push(subregionVals[n]); break }
          else if (m === (xlabs.length-1)) { // no match so assume missing vals at xlabs[m] - unused fields in shapefile cause unwanted null vals 
                                              subregionValsY.push(null); subregionCIL.push(null); subregionCIU.push(null); } 
        }
      }
    }
  }
  for (var i=0; i<xlabs.length; i++){
    bgdc2.push(plot.Background2Colour); bdrc1.push(plot.Line1Colour); bdrc2.push(plot.Line2Colour); bdrc3.push(plot.Line3Colour); pr.push(plot.MarkerSize)
  }
  bgdc2[geodataID-timeRefIndex] = plot.HighlightColour;
  bdrc2[geodataID-timeRefIndex] = plot.HighlightColour;
  bdrc1[geodataID-timeRefIndex] = plot.HighlightColour;
  pr[geodataID-timeRefIndex] = plot.HighlightSize;
  //ymax = Math.max.apply(Math, subregionValsY) * (1+1E-10); // add a little so plotted points aren't on max y-axis limit
  ymax = ( (typeof layerOps.timePlot.ymax==='undefined') ? Math.max.apply(Math, subregionValsY)*(1+1E-10) : layerOps.timePlot.ymax );
  beginYAtZero = layerOps.timePlot.beginYAtZero;
  dataMean = { label:layerOps.regionNames.country, data:meanVals, fill:true, pointRadius:pr,
               backgroundColor:plot.Background1Colour, borderColor:plot.Line1Colour, pointBorderColor:bdrc1 };
  dataY =    { label:label4subregion, data:subregionValsY, fill:false, pointRadius:pr,
               backgroundColor:plot.Background2Colour, borderColor:plot.Line2Colour, pointBackgroundColor:bgdc2, pointBorderColor:bdrc2 };
  dataCIL =  { label:ops.timeData.CIname, data:subregionCIL, fill:false, pointRadius:pr,
               backgroundColor:plot.Background3Colour, borderColor:plot.Line3Colour, pointBorderColor:bdrc3 };
  dataCIU =  { label:'UCI', data:subregionCIU, fill:'-1', pointRadius:pr,
               backgroundColor:plot.Background3Colour, borderColor:plot.Line3Colour, pointBorderColor:bdrc3 };
  if (twolines===true) {
    nullMean = meanVals.every(function (e){return e===null});
    //data = (meanVals.length>0 && subregionCIL.length===0) ? [dataMean,dataY] : [dataCIL,dataY,dataCIU];
    if (meanVals.length>0 && subregionCIL.length===0){
      data = (nullMean===true) ? [dataY] : [dataMean,dataY];
    } else {
      data = [dataY,dataCIL,dataCIU];
    }
  } else {
    data = [dataMean];
  }

  if (typeof timeseriesChart!=='undefined') { timeseriesChart.destroy() }
  if (subregionValsY.length===0 && twolines===true) { displaySlider(false,null,null,null); return }  // no data reported
  xticks = (typeof ops.timeData.xlabs!=='undefined') ? ops.timeData.xlabs : xlabs;
  if (typeof ops.timeData.highlight!=='undefined') { myhltXrange.highlight.padX = hltMark }
  timeseriesChart = new Chart(ctx, {  type: 'derivedLine',
                                      data: { labels:xticks, datasets:data, hltXrange:myhltXrange },
                                      options: { scales:{yAxes: { 
                                                          ticks:{beginAtZero:beginYAtZero, suggestedMax:ymax},
                                                          title:{display:true, text:layerOps.units.unicode} },
                                                         xAxes: {
                                                          ticks:{autoSkip:false, maxRotation:30, minRotation:30}, 
                                                          title:{display:displayXlabel, text:layerOps.units.xlab} }
                                                },
					                                      plugins:{
                                                      title:title,
                                                      filler:{propagate:false},
                                                      legend:{display:true, labels:{filter:function(legendItem, data){ return legendItem.datasetIndex<2 } } },
						                                    },
                                                animation:false,responsive:false,
                                                elements:{ line: {tension:0} },
                                      },
                                      lineAtIndex: {t:(geodataID-timeRefIndex), c:plot.HighlightColour} });
  displaySlider(true, ops, layerOps, xlabs[geodataID-timeRefIndex]);
  document.getElementById('slidertime').value = xticks[geodataID-timeRefIndex];
}
function ClearTimeSeriesPlot() {
  if ( canvasExists===true && typeof timeseriesChart!=='undefined') { timeseriesChart.destroy() }
}


// *** Time slider ***
var timeSliderListener = false;
function displaySlider(show, ops, layerOps, val) {  // needed when <input type="range"> doesn't exist so that time arrows are shown
  var r,
      p = document.getElementById('playpause'),           // assumes html element this id
      q = document.getElementById('playpausehide');       // assumes html element this id
      t = document.getElementById('slidertime'),
      l = document.getElementById('sliderlabel'),
      x = document.getElementById('sliderholder'),        // assumes html element this id
      y = document.getElementById('sliderholderhide');    // assumes html element this id
  if (show===true) {
    p.style.display = 'block';
    q.style.display = 'none';
    x.style.display = 'block';
    y.style.display = 'none';
    t.style.color = layerOps.timePlot.HighlightColour;
    l.style.color = layerOps.timePlot.HighlightColour;
    l.innerHTML   = layerOps.sliderlabel;
    if ( rangeExists===true ) {
      r = document.getElementById('sliderrange');
      r.min = ops.timeData.timeseriesMin;
      r.max = ops.timeData.timeseriesMax;
      r.step = ops.timeData.timeseriesStep;
      setTimeout( function(){ r.value=val }, 40 );     // setTimeout fixes old browser bug where r.value=val updates before r.min, r.max and r.step are set 
    }
  } else {
    p.style.display = 'none';
    q.style.display = 'block';
    x.style.display = 'none';
    y.style.display = 'block';
  }
}
var sliderPrevious=-1;
function addLayerWithSliderValue(i) {
  //if (typeof currentLayer!=='undefined' && currentLayer.layer.options.timeData.timeseries===true) { }
  if ( sliderPrevious===currentLayer.layer.options.i || sliderPrevious===-1 ) {  // fixes IE bug - stops IE loading layer twice when layer group changes
    map.addLayer( geodata[i-currentLayer.layer.options.timeData.timeseriesMin+currentLayer.layer.options.timeRefId] );
  }
  sliderPrevious = currentLayer.layer.options.i;
  timeSliderListener = true;
}
function createSlider() {
  var l = document.createElement('label'),
      o = document.createElement('input'),
      x = document.createElement('input'),
      a = document.createElement('div'),
      b = document.createElement('div'),
      playL = document.createElement('div'),
      playT = document.createElement('div'),
      playR = document.createElement('div'),
      playS = document.createElement('div'),
      playDx = document.createElement('select'),
      playDy = document.createElement('option'),
      labs = ["0. fastest","1. ","2. ","3. ","4. default","5. ","6. ","7. ","8. ","9. slowest"],
      vals = [50          ,100  ,150  ,200  ,250         ,300  ,350  ,400  ,450  ,500];

  l.for = "slidertime";
  l.id = "sliderlabel";

  o.type = "text";
  o.id = "slidertime";
  o.tabIndex = "-1";

  if ( rangeExists===true ) {      // construct element provided browser supports <input type="range">
    x.type = "range";
    x.id = "sliderrange";
    x.name = "slider";
    x.tabIndex = "0";
    if ( L.Browser.ie===true || L.Browser.edge===true ) {
      x.onchange = function(){ addLayerWithSliderValue(this.value) };
    } else {
      x.oninput  = function(){ addLayerWithSliderValue(this.value) };
    }
  }
  
  a.id = "arrow-left";
  //a.tabIndex = "0";
  a.onclick     = function(){ addLayerWithArrows(-1) };
  //a.onkeydown   = function(){ addLayerWithArrows(-1) };
  a.onmouseover = function(){ this.style.borderRightColor="#007ac3" };
  a.onfocus     = function(){ this.style.borderRightColor="#007ac3" };
  a.onmouseout  = function(){ this.style.borderRightColor="#b41019" };
  a.onblur      = function(){ this.style.borderRightColor="#b41019" };
  
  b.id = "arrow-right";
  //b.tabIndex = "0";
  b.onclick     = function(){ addLayerWithArrows(1) };
  //b.onkeydown   = function(){ addLayerWithArrows(1) };
  b.onmouseover = function(){ this.style.borderLeftColor="#007ac3" };
  b.onfocus     = function(){ this.style.borderLeftColor="#007ac3" };  
  b.onmouseout  = function(){ this.style.borderLeftColor="#b41019" };
  b.onblur      = function(){ this.style.borderLeftColor="#b41019" };

  playL.id = "arrow-left-play";
  playL.onclick     = function(){ playStop=true; setTimeout( function(){playStop=false; addLayerAuto(-1)}, vals[vals.length-1]+30 ) };
  playL.onmouseover = function(){ this.style.borderRightColor="#999" };
  playL.onmouseout  = function(){ this.style.borderRightColor="#007ac3" };
  playT.id = "playT";
  playT.innerHTML = "Play";
  playR.id = "arrow-right-play";
  playR.onclick     = function(){ playStop=true; setTimeout( function(){playStop=false; addLayerAuto(1)}, vals[vals.length-1]+30 ) };
  playR.onmouseover = function(){ this.style.borderLeftColor="#999" };
  playR.onmouseout  = function(){ this.style.borderLeftColor="#007ac3" };
  playS.id = "playS";
  playS.onclick     = function(){ playStop=true };
  playS.onmouseover = function(){ this.style.backgroundColor="#999" };
  playS.onmouseout  = function(){ this.style.backgroundColor="#007ac3" };
  playDx.onchange = function () { playSpeed=this.value };
  playDx.style.display = 'inline-block';
  playDx.style.color = "#007ac3";
  playSpeed = vals[4];                // default play speed
  playDy.text = "Select Speed";
  playDy.value = vals[4];
  playDx.appendChild(playDy);
  for (var i=0; i<labs.length; i++) {
    playDy = document.createElement('option');
    playDy.text = labs[i];
    playDy.value = vals[i];
    playDx.appendChild(playDy);
  }

  document.getElementById('sliderholder').appendChild(l);
  document.getElementById('sliderholder').appendChild(o);
  if ( rangeExists===true ) { document.getElementById('sliderholder').appendChild(x) }
  document.getElementById('sliderholder').appendChild(a);
  document.getElementById('sliderholder').appendChild(b);

  document.getElementById('playpause').appendChild(playL);
  document.getElementById('playpause').appendChild(playT);
  document.getElementById('playpause').appendChild(playR);
  document.getElementById('playpause').appendChild(playS);
  document.getElementById('playpause').appendChild(playDx);
  
  document.getElementById('slidertime').style.lineHeight = "32px";
  document.getElementById('sliderlabel').style.lineHeight = "32px";
  document.getElementById('playpause').style.lineHeight = "18px";

  displaySlider(false,null,null,null);
}


// *** functions called from html to add/remove map layers ***
function addDropdown( divName, k ) {  // add subset dropdown elements which are needed
  var y,
      a = 0,
      x = document.getElementById(divName);
  for (var i=0; i<k.length; i++) {
    if ( typeof x.options[i]!=='undefined' && x.options[i]!==null ) { // replace existing elements
      x.options[i].text = k[i];
      x.options[i].value = k[i];
    } else {                                  // add new elements
      y = document.createElement('option');   // need only 'x.appendChild(new Option(k[i],k[i]));' instead of all 4 lines but is it cross-browser compatible?
      y.text = k[i];
      y.value = k[i];
      x.appendChild(y);
    }
    a = a+1;
  }
  removeDropdownElements( 'control2dropdown', a );
  document.getElementById('controlOptionalDropDown').style.display = 'inline-block';
}
function removeDropdownElements( divName, val ) { // delete subset dropdown elements which are no longer needed
  var x = document.getElementById(divName);
  for ( var j=val; j<10000; j++ ) {
    if ( typeof x.options[j]!=='undefined' && x.options[j]!==null ){
      x.removeChild( x[j] );
    } else { 
      break;
    }
  }
}  
function chooseLayer(n) {
  if (n==="") { clearAllLayers(); return; }
  for ( var i=0; i<geodata.length; i++ ) {
    if ( n===geodata[i].options.layerName ) { 
      if ( typeof geodata[i].options.subsetOfLayers!=='undefined' && geodata[i].options.subsetOfLayers.length>0 ) {
        addDropdown('control2dropdown', geodata[i].options.subsetOfLayers);
      } else {
        removeDropdownElements( 'control2dropdown', 0 );
        document.getElementById('controlOptionalDropDown').style.display = 'none';
      }
      map.addLayer(geodata[i]);
      //setTimeout( function(){ adjustPosPnts() }, 200);
      break;
    }
  }
}
function chooseBaseLayer(n) {
  if (map.hasLayer(baseMaps[n])===false) {
    for (var i=0; i<Object.keys(baseMaps).length; i++) { map.removeLayer( baseMaps[Object.keys(baseMaps)[i]] ) }  // as a fail-safe close 'all'
    map.addLayer(baseMaps[n]);
  }  
}
var cleared = false;
function clearAllLayers() {
  cleared = true; currentHighlight = undefined;
  for (var j=0; j<geodata.length; j++) { map.removeLayer(geodata[j]) }    // as a fail-safe close 'all' open layers (instead of just the current layer)
  if (typeof markerLayer!=='undefined') { map.removeLayer(markerLayer) }
  if (typeof borderLayer!=='undefined') { map.removeLayer(borderLayer) }
  if ( radioNotDropdown===true ) {
    var element = document.getElementsByName('layer');                    // this assumes html has the name=layer element
    for (var i=0; i<element.length; i++) { element[i].checked = false  }  // clear all layer radio buttons
  } else {
    document.getElementById("control1dropdown").options[0].selected = 'selected'; // select first option on dropdown menu
  }
  removeDropdownElements( 'control2dropdown', 0 );
  document.getElementById('controlOptionalDropDown').style.display = 'none';
  map.closePopup();
}
function addLayerWithArrows(id) {
  var i = currentLayer.layer.options.geodataID;
  if ( (i+id)<geodata.length && (i+id)>-1 && geodata[i].options.timeRefId===geodata[i+id].options.timeRefId ) {
    timeSliderListener = true;
    map.addLayer(geodata[i+id]);
  }
}
var playStop = false,
    playSpeed = 100;
function addLayerAuto(id) {   // automatically play through group layers
  var i = currentLayer.layer.options.geodataID,
      j = 0, 
      jMax = geodata.length; // fail-safe in case recursion goes wrong!
  function f() {
    if ( (i+j*id)<geodata.length && (i+j*id)>-1 && geodata[i].options.timeRefId===geodata[i+j*id].options.timeRefId && j<jMax && playStop===false) {
      timeSliderListener = true;
      map.addLayer(geodata[i+j*id]);
      setTimeout( f, playSpeed );
    } else {
      playStop = false;
      return;
    }
    j=j+1;
  }
  f();
}
function reloadPage() { location.reload(true) }                         // force webpage to reload from the server (rather than cache)


// *** create layer types and baselayers radio buttons or dropdowns in html ***
function getFriendlyNames() {   // the actual names in dropdown menu
  var names = [];
  for (var i=0; i<layerdata.length; i++) { names.push(layerdata[i].friendlyname) }
  return names;
}
function createRadioButtons(divName, k, radioName, anchorClassName, noneChecked, base) {
  var x, y, S, Sa,
      ctrljsflow = false,
      niceNames,
      Sb = "\')";
  if (divName==='control1radioButtons') { niceNames = getFriendlyNames() }
  if ( document.getElementById(divName)===null ){ return }
  if ( typeof radioNotDropdown==='undefined' ) { radioNotDropdown=false }
  if ( radioNotDropdown===false && radioName==='layer' ) { 
    x = document.createElement('select');
    x.id='control1dropdown';
    x.onchange = function () { chooseLayer(this.value) };
    y = document.createElement('option');
    y.text = 'Select Map';
    y.value = '';
    x.appendChild(y);
  }
  if ( radioName==='layertype' ) {
    x = document.createElement('select');
    x.id='control2dropdown';
    x.onchange = function () {  };                 // *****************************  when ready need to add a function in here (like 9 lines above here)
  }
  for (var i=0; i<k.length; i++) {
    if ( ( radioNotDropdown===true && radioName==='layer') || radioName==='baselayer' ){
      x = document.createElement('input');
      x.type = 'radio';
      x.id = k[i];
      x.name = radioName;
      x.autocomplete = 'off';
      x.className = 'pointerhand';
      if ( base===true ){
        x.onclick = function(){ chooseBaseLayer(this.id) };
        Sa = "javascript:chooseBaseLayer(\'";
      } else {
        x.onclick = function(){ chooseLayer(this.id) };
        Sa = "javascript:chooseLayer(\'";
      }
      if ( noneChecked===true ) {
        x.checked=false;
      } else {
        ( i===0 && typeof secondMap==='undefined' )? x.checked=true : x.checked=false; // check first radio button in listed in k
        if ( i===1 && typeof secondMap!=='undefined' && secondMap===true && radioName==='baselayer') { x.checked=true } // check 2nd base layer radio button
      }
      document.getElementById(divName).appendChild(x);
      y = document.createElement('a');
      S = '';
      S = S.concat(Sa,k[i],Sb);
      y.className = anchorClassName;
      y.href = S;
      //y.innerHTML = k[i];
      (divName==='control1radioButtons') ? y.innerHTML=niceNames[i] : y.innerHTML=k[i];
      y.style.margin = '0 5px'; 
      document.getElementById(divName).appendChild(y);
    } else if ( radioNotDropdown===false || radioName==='layertype' ){
      y = document.createElement('option');
      (divName==='control1radioButtons') ? y.text=niceNames[i] : y.text=k[i];
      y.value = k[i];
      x.appendChild(y);
      ctrljsflow = true; // secure thread: ensure div elements are in correct order otherwise if(ctrljsflow) could be run too early in some browsers
    }
  }
  if ( ctrljsflow ) { 
    document.getElementById(divName).appendChild(x);
  }
}


// *** Search for a location using google search api ***
if ( userDefinedBaseMaps===false ) {

   L.Control.GoogleSearch = L.Control.extend( {
   onAdd: function() {
         //var y = L.DomUtil.create('div', 'searchboxholder');  y.innerHTML   = '<form><input id="placeSearch" type="text" value="Some text"></form>';
       var y = L.DomUtil.create('div', 'searchboxholder'), //need to put <input> inside <div> otherwise can't get focus in mobile Firefox
           x = document.createElement("input"),
           clear = "",
           txt = "Search Location";
       x.id = "placeSearch";
       x.name = "gSearch";
       x.type = "text";
       x.placeholder = txt;
       x.onblur  = function(){ this.placeholder=txt };
       if (L.Browser.webkit===true || L.Browser.opera12===true || L.Browser.gecko===true || L.Browser.mobileGecko===true ) {
         x.onclick = function(){ this.select(); this.value=clear; this.placeholder=clear };  // good for Firefox (win+android), Chrome(win+android), Opera(win)
       } else if ( L.Browser.ie===true || L.Browser.edge===true ) {                         
         x.onclick = function(){ this.focus(); this.value=clear };                           // good for IE(win) and edge(win)
       } else if ( L.Browser.mobileOpera===true ) {
         x.onclick = function(){ this.select() };                                            // good for Opera(android)
       } else {
         x.onclick = function(){ this.select() };                                            // hope for the best espeically on mobile browsers!
       }                                                                                     // fault stops native android browser from using search
       y.style.border = '4px solid #fff';
       y.appendChild(x);    
       return y;
     }
   } );
   new L.Control.GoogleSearch({ position:'topright'}).addTo(map);

   var searchBox = new google.maps.places.SearchBox( document.getElementById('placeSearch') );  // reference to google search api
   searchBox.addListener('places_changed', function() {
     var places = searchBox.getPlaces();
     if ( places.length===0 ) { return }
     new L.marker(          [ places[0].geometry.location.lat(), places[0].geometry.location.lng() ] ).addTo(map);
     map.panTo( new L.LatLng( places[0].geometry.location.lat(), places[0].geometry.location.lng() ) );
   });
}


// *** map scale control (miles) ***
if (typeof mapUnits==='undefined') { mapUnits = true }
new L.control.scale({ position:'bottomleft', metric:mapUnits }).addTo(map);


// *** zoom control ***
new L.Control.Zoom({ position: 'bottomleft' }).addTo(map);
