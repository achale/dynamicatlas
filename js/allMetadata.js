addEventListener('load',
                   function(){
                     setTimeout(function(){
                       var ln;              // full layername
                       document.getElementById('loadholderbacking').style.display='none';
                       if (typeof layerId==='undefined') {
                         chooseLayer('Jan 17');
                       } else {
                         for ( var i=0; i<geodata.length; i++ ) {
                           ln = geodata[i].options.layerName;
                           if ( layerId===ln ) { 
                             map.addLayer(geodata[i]);
                             break;
                           }
                         }
                       }
                     }, 2000);
                   });

   var radioNotDropdown = true;
   var userDefinedBaseMaps = false;
   var InitialMapCenterLatLng = [7.8731, 80.7718];
   var InitialmapZoom = 8;
   var mapUnits = true;
   var secondMap = true;
   var userDefinedBaseMaps = true;
   var layerdata = [{ 
    filename: "data/SriLankaDengue20172018.geojson",
    layerName: ["Jan 17","Feb 17","Mar 17","Apr 17","May 17","Jun 17","Jul 17","Aug 17","Sep 17","Oct 17","Nov 17","Dec 17","Jan 18","Feb 18","Mar 18","Apr 18","May 18","Jun 18","Jul 18","Aug 18","Sep 18","Oct 18","Nov 18","Dec 18"],
    geojsonname: ["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24"],
    geojsongeom: "DISTRICT_N",
    geojsonExtraInfo: "Population",
    friendlyname: "dengue 2017-2018",
    mapPosition: { centerLatLng:[7.8731, 80.7718], zoom:8 },
    regionNames: { country:"SriLanka", area:"District" },
    colgrades: { Ints:15, Inis:0, Num:undefined,
                 Vals:["#fff7db","#ffeda0","#fed976","#fd8d3c","#fc4e2a","#e31a1c","#b10026","#800026"] },
    legend: true,
    sliderlabel: "",
    mapStyles: { weight:1, opacity:1, color:"#aaa", fillOpacity:0.7, smoothFactor:1, radius:undefined },
    noDataColour: "rgba(0, 0, 0, 0.3)",
    featurehltStyle: { weight:5 },
    timeData: { timeseries: true, timeseriesMin:1, timeseriesMax:24, timeseriesStep:1, 
                highlight:undefined, xlabs:["Jan 17","Feb 17","Mar 17","Apr 17","May 17","Jun 17","Jul 17","Aug 17","Sep 17","Oct 17","Nov 17","Dec 17","Jan 18","Feb 18","Mar 18","Apr 18","May 18","Jun 18","Jul 18","Aug 18","Sep 18","Oct 18","Nov 18","Dec 18"] },
    meandata: [45.71,42.35,66.81,56.1,56.14,82.62,135.74,78.67,36.48,28.05,37.22,45.79,30.16,19.8,16.24,12.35,16.32,25.4,29.58,18.45,8.87,8.22,19.03,24.1],
    radioButtonValue: "Jan 17",
    timePlot: { Background1Colour:"rgba(0,0,255,0.1)", Line1Colour:"rgba(0,0,255,0.2)",
                Background2Colour:"#007ac3", Line2Colour:"#007ac3",
                MarkerSize:3, HighlightColour:"#b41019", HighlightSize:5,
                ymax: undefined },
    subsetOfLayers: undefined,
    layerMarker: undefined,
    mapBoundary: { filename:"data/border.geojson",  borderStyle:{color:"#aaa", weight:2} },
    units: {html:"cases/10<span style=\"vertical-align:super\">5</span>pop", unicode:"cases / 10\u2075 pop", xlab:"years: 2017 - 2018"},
    downloadData:"data/SriLankaDengue20172018.csv",
    popupBox: false
    }]
